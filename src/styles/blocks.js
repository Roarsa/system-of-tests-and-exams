import { colors } from 'styles/constants';

export const panel = `
  border: 1px solid ${colors.shadowAlto};
  border-radius: 3px;
  box-shadow: 0 1px 2px ${colors.shadowConcrete};
  background-color: ${colors.white};
`;

export const border = `
  border: 1px solid ${colors.shadowAlto};
  border-radius: 3px;
`;

export default {
  panel,
  border,
};
