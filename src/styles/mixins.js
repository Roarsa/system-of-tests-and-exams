const size = (width, height) => `
  width: ${width};
  height: ${height || width};
`;

const bg = (path, bgSize, repeat) => `
  background-image: url(images/"${path}");
  background-size: ${bgSize || 'contain'};
  background-repeat: ${repeat ? 'repeat' : 'no-repeat'};
`;

const shadow = color => `
  box-shadow: 0 0 0 100px ${color} inset;
`;

const center = (props = { position: 'absolute', axis: '', transform: '' }) => `
  position: ${props.position};
  top: 50%;
  left: 50%;
  transform: translate${props.axis}(-50%, -50%) ${props.transform};
`;

export default {
  size,
  bg,
  shadow,
  center,
};
