import { createGlobalStyle } from 'styled-components';
import { normalize } from 'styled-normalize';
import { colors, fontFamily, fontWeight } from 'styles/constants';

export const GlobalStyle = createGlobalStyle`
  ${normalize}

  // You can continue writing global styles here
  body {
    padding: 0;
    font-family: ${fontFamily.roboto};
    font-weight: ${fontWeight.regular};
    font-size: 13px;
    color: ${colors.mineShaft};
    background-color: ${colors.wildSand};
  }

  body.openModal {
    overflow: hidden;
  }

  div,
  img,
  p,
  ul,
  li,
  a {
    box-sizing: border-box;
  }

  p {
    padding: 0;
    margin: 0;
  }

  img {
    display: block;
  }

  ul {
    padding: 0;
    margin: 0;
  }

  li {
    list-style-type: none;
  }

  input,
  button {
    outline: none;
    border: none;
  }

  button {
    padding: 0;
    background-color: unset;
  }
`;
