export const colors = {
  black: '#000',
  white: '#fff',
  mineShaft: '#333',
  dustyGray: '#999',
  wildSand: '#f5f5f5',
  alabaster: '#fcfcfc',
  fiord: '#455a64',
  orangePeel: '#ff9800',
  indochine: '#c97b00',
  doveGray: '#666',
  shadowMineShaft: 'rgba(0,0,0,0.8)',
  shadowGray: 'rgba(0,0,0,0.5)',
  shadowSilver: 'rgba(0,0,0,0.25)',
  shadowDarkAlto: 'rgba(0,0,0,0.15)',
  shadowAlto: 'rgba(0,0,0,0.125)',
  shadowConcrete: 'rgba(0,0,0,0.05)',
  shadowAlabaster: 'rgba(0,0,0,0.03)',
  shadowLightAlabaster: 'rgba(0,0,0,0.01)',
};

export const fontFamily = {
  roboto: '"Roboto", sans-serif',
};

export const fontWeight = {
  thin: '100',
  extralight: '200',
  ultralight: '200',
  light: '300',
  book: '400',
  normal: '400',
  regular: '400',
  roman: '400',
  medium: '500',
  semibold: '600',
  demibold: '600',
  bold: '700',
  extrabold: '800',
  ultrabold: '800',
  black: '900',
  heavy: '900',
};

export const mediaBreakpoints = {
  mobile: '767px',
  tablet: '1024px',
  laptop: '1280px',
  desktopS: '1440px',
  desktopL: '1921px',
};

export const media = {
  mobile: `(max-width: ${mediaBreakpoints.mobile})`, // to 767
  tablet: `(max-width: ${mediaBreakpoints.tablet})`, // to 1024
  laptop: `(max-width: ${mediaBreakpoints.laptop})`, // to 1280
  desktopS: `(max-width: ${mediaBreakpoints.desktopS})`, // to 1440
  desktopL: `(min-width: ${mediaBreakpoints.desktopL})`, // from 1921
  screen: `screen and (pointer: fine)`,
};

export const tr = {
  hover: '0.15s',
};

export default {
  colors,
  fontFamily,
  fontWeight,
  media,
};
