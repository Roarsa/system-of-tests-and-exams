import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet-async';

import config from 'config';

import AppRouter from 'components/AppRouter';

import { GlobalStyle } from 'styles';

const App = ({ routes }) => (
  <div>
    <Helmet {...config.app} />
    <GlobalStyle />
    <AppRouter routes={routes} />
  </div>
);

App.propTypes = {
  routes: PropTypes.array,
  browser: PropTypes.object,
};

export default App;
