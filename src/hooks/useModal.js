import { useEffect, useCallback } from 'react';

function useModal(closeHandler) {
  const boardHandler = useCallback(e => {
    if (e.key === 'Escape') {
      closeHandler();
    }
  }, []);

  useEffect(() => {
    document.addEventListener('keyup', boardHandler);
    const body = document.body.classList;
    body.add('openModal');
    return () => {
      body.remove('openModal');
      document.removeEventListener('keyup', boardHandler);
    };
  }, []);
}

export default useModal;
