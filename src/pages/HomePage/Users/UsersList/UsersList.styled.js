import styled from 'styled-components';
import logo from 'images/icons/logo.svg';

export default {
  Title: styled.h1`
    font-size: 1.5em;
    text-align: center;
    color: palevioletred;
  `,

  ImgOut: styled.img`
    width: 100px;
    height: 100px;
  `,

  Img: styled.img.attrs(() => ({
    src: logo,
    width: '100',
    height: '100',
  }))`
    width: 100px;
    height: 100px;
    background-color: ${props => props.bgColor || 'black'};
  `,

  Bg: styled.div`
    background-image: url(${logo});
    background-size: contain;
    background-repeat: no-repeat;
    width: 100px;
    height: 100px;
  `,
};
