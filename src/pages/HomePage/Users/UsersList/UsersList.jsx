import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import S from './UsersList.styled';
import logoIcon from 'images/icons/logo.svg';

const UsersList = ({ list }) => (
  <div>
    <S.Title>Users List</S.Title>
    <S.Img />
    <S.ImgOut src={logoIcon} />
    <svg>
      <use xlinkHref={`${logoIcon}#logo`} />
    </svg>
    <S.Bg />
    <ul>
      {list.map(user => (
        <li key={user.id}>
          <Link to={`/users/${user.id}`}>{user.name}</Link>
        </li>
      ))}
    </ul>
  </div>
);

UsersList.propTypes = {
  list: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    })
  ),
};

UsersList.defaultProps = {
  list: [],
};

export default UsersList;
