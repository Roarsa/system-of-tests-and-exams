import React from 'react';
import { Helmet } from 'react-helmet-async';

const NotFound = () => (
  <div>
    <Helmet title="Oops" />
    <p>Oops, Page was not found!</p>
  </div>
);

export default NotFound;
